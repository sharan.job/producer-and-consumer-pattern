﻿using System;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Producer_Consumer_Pattern
{
    class Program
    {
        //A channel is simply a data structure that’s used to store produced data for a consumer to retrieve, 
        //and an appropriate synchronization to enable that to happen safely, while also enabling appropriate
        //notifications in both directions. There is a multitude of possible design decisions involved.
        static async Task Main()
        {
            var c = Channel.CreateUnbounded<int>();
            _ = Task.Run(async delegate
            {
                for (int i = 0; ; i++)
                {
                    await Task.Delay(1000);
                    await c.Writer.WriteAsync(i);

                }
            });

            while (true)
            {
                await Task.Delay(2000);
                Console.WriteLine(await c.Reader.ReadAsync());
            }
        }
    }
}
